/*
 * Author:
 * Kamil Mielnik 
 * mielnik@student.agh.edu.pl
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TRUE 1
#define FALSE 0

/* STRUCTURES */
struct properties
{
	char width;
	char height;
	short agentsCount;
	short lastAgent;
};

struct point
{
	char x;
	char y;
};

struct agent
{
	short id;
	struct point start;
	struct point end;
};

/* GLOBAL VARIABLES */
char DEBUG;
struct properties properties;
struct agent *agents;
char **map;
char **originalMap;
int moveCounter;

/* PARSING FUNCTIONS */
void parseInput()
{
	int buffer;
	scanf("%d", &buffer);
	properties.width = buffer;

	scanf("%d", &buffer);
	properties.height = buffer; 

	scanf("%d", &buffer);
	properties.agentsCount = buffer;
	properties.lastAgent = properties.agentsCount - 1;

	agents = (struct agent *)malloc(sizeof(struct agent) * properties.agentsCount);

	int agent = 0;
	while(agent < properties.agentsCount)
	{
		agents[agent].id = agent + 1;

		scanf("%d", &buffer);
		agents[agent].start.x = buffer - 1;

		scanf("%d", &buffer);
		agents[agent].start.y = buffer - 1;

		scanf("%d", &buffer);
		agents[agent].end.x = buffer - 1;

		scanf("%d", &buffer);
		agents[agent].end.y = buffer - 1;
		++agent;
	}
}

/* MAP FUNCTIONS */
char **createMap()
{
	char **map = (char**)malloc(sizeof(char*) * properties.width);
	int i, j;
	for(i = 0; i < properties.width; ++i)
	{
		map[i] = (char*)malloc(sizeof(char) * properties.height);
		for(j = 0; j < properties.height; ++j)
			map[i][j] = 0;
	}

	for(i = 0; i < properties.agentsCount; ++i)
	{
		map[agents[i].start.x][agents[i].start.y] = agents[i].id;
		map[agents[i].end.x][agents[i].end.y] = agents[i].id;
	}
		
	return map;
}

void printMap(char **map)
{
	int i, j;
	for(j = 0; j < properties.height; ++j)
	{
		for(i = 0; i < properties.width; ++i)
			printf("%d ", map[i][j]);
		printf("\n");
	}
	printf("\n");
}

/* RESULT FUNCTIONS */
void traverse(short agent, char x, char y)
{
	struct agent current_agent = agents[agent];
	short id = current_agent.id;

	if(current_agent.end.x == x && current_agent.end.y == y)
		return;

	printf("%d %d ", x + 1, y + 1);

	map[x][y] *= -1;
	if(x < properties.width - 1 && map[x + 1][y] == id)
	{
		printf("%d %d\n", 1, 0);
		traverse(agent, x + 1, y);
	}
	else if(y < properties.height - 1 && map[x][y + 1] == id)
	{
		printf("%d %d\n", 0, 1);
		traverse(agent, x, y + 1);
	}
	else if(x > 0 && map[x - 1][y] == id)
	{
		printf("%d %d\n", -1, 0);
		traverse(agent, x - 1, y);
	}
	else if(y > 0 && map[x][y - 1] == id)
	{
		printf("%d %d\n", 0, -1);
		traverse(agent, x , y - 1);
	}
}

void countMoves(){
	moveCounter = 0;

	short i, j;
	for(i = 0; i < properties.width; ++i)
		for(j = 0; j < properties.height; ++j)
			if(map[i][j] != 0)
				++moveCounter;

	moveCounter -= properties.agentsCount; //do not count starting fields
}

void printResult()
{
	countMoves();
	printf("%d\n", moveCounter);

	short i;
	for(i = 0; i < properties.agentsCount; ++i)
	{
		traverse(i, agents[i].start.x, agents[i].start.y);
	}
}

char solve(short agent, char x, char y);

/* SIMPLE HEURISTICS */
void reverseHorizontal(char y, char left, char right)
{
	char i = right;
	while(i > left)
	{
		map[i][y] = originalMap[i][y];
		--i;
	}
}

void reverseVertical(char x, char up, char down)
{
	char i = down;
	while(i > up)
	{
		map[x][i] = originalMap[x][i];
		--i;
	}
}

char tryTurn(short id, char from_x, char from_y, char to_x, char to_y, char x, char y)
{
	if(map[x][y] != 0)
		return FALSE;

	map[x][y] = id;

	char i, j;

	char x_left, x_right;
	if(from_x < to_x)
	{
		x_left = from_x;
		x_right = to_x;
	}
	else
	{
		x_left = to_x;
		x_right = from_x;
	}

	char y_top, y_bottom;
	if(from_y < to_y)
	{
		y_top = from_y;
		y_bottom = to_y;
	}
	else
	{
		y_top = to_y;
		y_bottom = from_y;
	}

	i = x_left + 1;
	while (i < x_right)
	{
		if(map[i][y] != 0) //field is already occupied
		{
			reverseHorizontal(y, x_left, i - 1);
			map[x][y] = originalMap[x][y];
			return FALSE;
		}
		map[i][y] = id;
		++i;
	}
		
	j = y_top + 1;	
	while(j < y_bottom)
	{
		if(map[x][j] != 0) //field is already occupied
		{
			reverseVertical(x, y_top, j - 1);
			reverseHorizontal(y, x_left, x_right);
			map[x][y] = originalMap[x][y];
			return FALSE;
		}
		map[x][j] = id;
		++j;
	}

	return TRUE;
}

void reverseTurn(short id, char from_x, char from_y, char to_x, char to_y, char x, char y)
{
	char x_left, x_right;
	if(from_x < to_x)
	{
		x_left = from_x;
		x_right = to_x;
	}
	else
	{
		x_left = to_x;
		x_right = from_x;
	}

	char y_top, y_bottom;
	if(from_y < to_y)
	{
		y_top = from_y;
		y_bottom = to_y;
	}
	else
	{
		y_top = to_y;
		y_bottom = from_y;
	}

	reverseVertical(x, y_top, y_bottom);
	reverseHorizontal(y, x_left, x_right);
}

char guess(short agent)
{
	if(agent == properties.agentsCount)	//no more agents
		return TRUE;

	struct agent current = agents[agent];
	short id = current.id;
	struct point start = current.start;
	struct point end = current.end;

	if(tryTurn(id, start.x, start.y, end.x, end.y, start.x, end.y))
	{
		if(guess(agent + 1))
			return TRUE;
		else
			reverseTurn(id, start.x, start.y, end.x, end.y, start.x, end.y);
	}
	
	if(tryTurn(id, start.x, start.y, end.x, end.y, end.x, start.y))
	{
		if(guess(agent + 1))
			return TRUE;
		else
			reverseTurn(id, start.x, start.y, end.x, end.y, end.x, start.y);
	}		
	
	return solve(agent, start.x, start.y);
}

//brute force:
char solve(short agent, char x, char y)
{
	struct agent current = agents[agent];
	short id = current.id;
	struct point end = current.end;
	map[x][y] = id;
	
	if(end.x == x && end.y == y)
	{
		if(guess(agent + 1))
			return TRUE;
	}
	else
	{
		if(x < properties.width - 1 && (map[x + 1][y] == 0 || (end.x == x + 1 && end.y == y)))
		{
			if(solve(agent, x + 1, y))
				return TRUE;
		}
		if(y < properties.height - 1 && (map[x][y + 1] == 0 || (end.x == x && end.y == y + 1)))
		{
			if(solve(agent, x, y + 1))
				return TRUE;
		}
		if(x > 0 && (map[x - 1][y] == 0 || (end.x == x - 1 && end.y == y)))
		{
			if(solve(agent, x - 1, y))
				return TRUE;
		}
		if(y > 0 && (map[x][y - 1] == 0 || (end.x == x && end.y == y - 1)))
		{
			if(solve(agent, x, y - 1))
				return TRUE;
		}
	}

	map[x][y] = originalMap[x][y];
	return FALSE;
}

int main(int argc, char* argv[])
{
	if(argc > 1)
		DEBUG = TRUE;
	else
		DEBUG = FALSE;
	

	parseInput();

	originalMap = createMap();
	map = createMap();

	if(DEBUG == TRUE)
	{
		clock_t start = clock();
		guess(0);
		clock_t end = clock();

		printResult();

		printf("\ncreated map: \n");
		printMap(originalMap);

		printf("final map: \n");
		printMap(map);

		float seconds = (float)(end - start) / CLOCKS_PER_SEC;
		printf("time elapsed: %lf\n", ((int)(1000 * seconds)) / 1000.0f);
		printf("\r\n");
	}
	else
	{
		guess(0);
		printResult();
	}

	return 0;
}
