#!/usr/bin/env python

# Projekt TOiZO -- Lato 2013
# Piotr Faliszewski

from sys import *
import os


viewer = True



def complain( err ):
  """Zglos blad i zakoncz program"""
  print err
  os._exit(1)






def readBoard( f ):
  """Wczytaj opis planszy z pliku f"""
  data = f.readlines()
  header = data[0].split()
  x = int(header[0])
  y = int(header[1])
  k = int(data[1])
  if( len( data ) < k+2 ):
    complain("Niepoprawny format wejscia: Za malo wierszy.")
  

  agents = []
  for i in range(k):
    agent = [ int(z)-1 for z in data[i+2].split() ]
    if( len(agent) != 4 ):
      complain("Niepoprawny format wejscia: Opis agenta %d" % (i+1))
    agents += [ agent ]

  board = []
  for i in range(y):
    board += [ [ (0,0) ]*x ]

  return (x,y,k,agents,board)






def readSolution( f, P ):
  """Wczytaj opis planszy z pliku f"""
  data = f.readlines()
  s = int( data[0] )
  if( len( data ) < s+1 ):
    complain("Niepoprawny format rozwiazania: Za malo wierszy.")
  
  for i in range(s):
    path = [ int(z) for z in data[i+1].split() ]
    if( len(path) != 4 ):
      complain("Niepoprawny format rozwiazania: Wiersz %d" % (i+2))

    (x,y,dx,dy) = (path[0], path[1], path[2], path[3])
    if( x < 1 or x > P[0] or y < 1 or y > P[1]):
      complain("Niepoprawny format rozwiazania: Wiersz %d, wspolrzedne (%d,%d)" % ((i+2), x,y))
    if( dx**2 + dy**2 != 1 ):
      complain("Niepoprawny format rozwiazania: Wiersz %d, krok (%d,%d)" % ((i+2), dx,dy))
        
    P[4][y-1][x-1] = (dx,dy)


  



def checkSolution( P ):
  """sprawdza rozwiazania; przerywa program jesli wystepuje blad"""
  agents  = P[3]
  board   = P[4]

  for (x,y,tx,ty) in agents:
    while( x != tx or y != ty ):
      (dx,dy) = board[y][x]
      if( dx**2 + dy**2 != 1):
        complain("Blad w trasach")
      board[y][x] = (0,0)
      x += dx
      y += dy




def printSolution( P ):
  """wypisuje rozwiazanie w czytelnej postaci"""
  (x,y,k) = (P[0],P[1],P[2])
  agents  = P[3]
  board   = P[4]

  pboard = []
  for i in range(y):
    pboard += [ ["."]*3*x ] 
    pboard += [ ["."]*3*x ] 
    pboard += [ ["."]*3*x ] 



  for yy in range(y):
    for xx in range(x):
      (dx,dy) = board[yy][xx]
      if( dx**2 + dy**2 != 0 ):
        for i in range(3):
          if( 3*yy+(i*dy)+1 >= 0 and 3*yy+(i*dy)+1 < 3*y ):
            if( 3*xx+(i*dx)+1 >= 0 and 3*xx+(i*dx)+1 < 3*x ):
              pboard[3*yy+(i*dy)+1][3*xx+(i*dx)+1] = "o"

  for i in range(len(agents)):
    (sx,sy,tx,ty) = agents[i]
    pboard[3*sy+1][3*sx+0] = "("
    pboard[3*sy+1][3*sx+1] = str(i+1)
    pboard[3*sy+1][3*sx+2] = ")"
    pboard[3*ty+1][3*tx+0] = "<"
    pboard[3*ty+1][3*tx+1] = str(i+1)
    pboard[3*ty+1][3*tx+2] = ">"

  

  for line in pboard:
    s = ""
    for ch in line:
      s += ch
    print s
  
 


if( len(argv) != 3 ):
  print "Wywolanie: "
  print "  python %s plansza rozwiazanie" % argv[0]
  os._exit(0)

try: 
  problem  = open(argv[1],"r")
  solution = open(argv[2],"r")

  P = readBoard( problem )
  readSolution( solution, P )

  if( viewer ):
    printSolution( P )

  checkSolution( P )
  print "OK"


except IOError:
  complain("Blad we/wy.")
except KeyError:
  complain("Blad")
except:
  complain("Blad")
